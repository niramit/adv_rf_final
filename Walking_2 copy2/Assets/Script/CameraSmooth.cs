﻿using UnityEngine;
using System.Collections;

public class CameraSmooth : MonoBehaviour {

	private Vector2 velocity;
	public float smoothTimeY;
	public float smoothTimeX;

	public GameObject Player;

	void Start () 
	{
		Player = GameObject.FindGameObjectWithTag ("Player");

	}

	void FixedUpdate () 
	{
		float posX = Mathf.SmoothDamp (transform.position.x, Player.transform.position.x, ref velocity.x, smoothTimeX);
		float posY = Mathf.SmoothDamp (transform.position.x, Player.transform.position.y, ref velocity.y, smoothTimeY);

		transform.position = new Vector3 (posX, posY, transform.position.z);
	}
}
