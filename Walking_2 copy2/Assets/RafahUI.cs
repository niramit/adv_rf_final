﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RafahUI : MonoBehaviour 
{
	public int DeathCount;
	public GameObject healthBar;
	public int Planks;
	public GameObject PlankText;
	// Use this for initialization
	void Start () {
		DeathCount = 1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("monsters") || other.gameObject.CompareTag ("spike")) {
			RectTransform healthRect = healthBar.GetComponent<RectTransform> ();
			healthRect.sizeDelta -= new Vector2 (35, 0);
			DeathCount++;
			if (DeathCount >= 9) {
				Application.LoadLevel (Application.loadedLevelName);
			}
		}
		if (other.gameObject.CompareTag ("Item")) {
			if (DeathCount > 2) {
				RectTransform healthRect = healthBar.GetComponent<RectTransform> ();
				healthRect.sizeDelta += new Vector2 (70, 0);
				DeathCount--;
				DeathCount--;
			} else if (DeathCount == 2) {
				RectTransform healthRect = healthBar.GetComponent<RectTransform> ();
				healthRect.sizeDelta += new Vector2 (35, 0);
				DeathCount--;
			}
			Destroy (other.gameObject, 0.1f);
		}

		if (other.gameObject.CompareTag ("Planks")) 
		{
			Planks = Planks + 1;
			Destroy (other.gameObject);
			Debug.Log ("get plank");


			if (Planks == 1) 
			{
				Debug.Log ("Plank = 1");
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "1 / 4";
			} 
			else if (Planks == 2) 
			{
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "2 / 4";
			} 
			else if (Planks == 3) 
			{
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "3 / 4";
			} 
			else 
			{
				GameObject.Find ("PlankText").GetComponent<Text> ().text = "4 / 4";
			}
		}
	}
}
