﻿using UnityEngine;
using System.Collections;

public class Snake : MonoBehaviour,IMonster  {

	private int Health;
	public Animator useAnimator;
	public Rigidbody2D rb;

	public GameObject Health1;
	public GameObject Health2;
	public GameObject Health3;
	public GameObject Health4;

	public GameObject HealthBG;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		Health = 4;
	}

	// Update is called once per frame
	void Update () 
	{

	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			useAnimator.SetBool ("hit", true);
		} 
	}
	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			useAnimator.SetBool ("hit", false);
		} 
	}
	public void hit()
	{
		print ("hit");
		Health--;
		if (Health == 3) 
		{
			Destroy (Health1.gameObject, 0.0f);
		} 
		if (Health == 2) 
		{
			Destroy (Health2.gameObject, 0.0f);
		}
		if (Health == 1) 
		{
			Destroy (Health3.gameObject, 0.0f);
		}
		if (Health == 0) 
		{
			Destroy (this.gameObject, 0.0f);
			Destroy (Health4.gameObject, 0.0f);
			Destroy (HealthBG.gameObject, 0.0f);
		}
	}
}
