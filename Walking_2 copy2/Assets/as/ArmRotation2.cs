﻿using UnityEngine;
using System.Collections;

public class ArmRotation2 : MonoBehaviour 
{
	public int rotationOffset = 0;
	public Char2D Player;

	void Update () 
	{
		Vector3 mousePos = Input.mousePosition;
		Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
		mousePos = mousePos - pos;
		if (Player.m_FacingRight == true) 
		{
			//Debug.Log ("Hun Kwa Laew");
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, Mathf.Atan2 (mousePos.y, mousePos.x) * Mathf.Rad2Deg - rotationOffset));
			Debug.Log (transform.rotation + " ");
			if (transform.rotation.x >= -0.7f)
			{
				Player.Flip();
			}
		} 
		else 
		{
			//Debug.Log ("Hun sai laew");
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, (Mathf.Atan2 (mousePos.y, mousePos.x) * - (Mathf.Rad2Deg - rotationOffset) - 180)));
			Debug.Log (transform.rotation + " ");
		}
	}

}