﻿using UnityEngine;
using System.Collections;

public class MadKingBulletHellMove : MonoBehaviour 
{

	private float MoveSpeed = 10f;

	void Update () 
	{
		transform.Translate (Vector3.left * Time.deltaTime * MoveSpeed);
		Destroy (gameObject, 5);
	}

}


