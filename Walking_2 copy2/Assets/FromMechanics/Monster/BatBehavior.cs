﻿using UnityEngine;
using System.Collections;

public class BatBehavior : MonoBehaviour
{
	private int state;
	public bool findRafa;

	void Start () 
	{
		state = 0;
		findRafa = false;
	}

	void Update () 
	{
		if (findRafa == true) 
		{
			StartCoroutine (fly());
		}
		if (state == 0) 
		{
			this.transform.Translate (new Vector3 (0, 0, 0) * Time.deltaTime);
		} 
		else if (state == 1) 
		{
			this.transform.Translate (new Vector3 (-5, -1, 0) * Time.deltaTime * 3.0f);
		} 
		else if (state == 2) 
		{
			this.transform.Translate (new Vector3 (-1, 1, 0) * Time.deltaTime * 3.0f);
		}
		findRafa = false;
	}

	IEnumerator fly()
	{
		state = 0;
		yield return new WaitForSeconds(0f);
		state = 1;
		yield return new WaitForSeconds(1.5f);
		state = 2;
		yield return new WaitForSeconds(1.5f);
		state = 0;

	}



}
