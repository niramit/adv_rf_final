﻿using UnityEngine;
using System.Collections;

public class RafahJump : MonoBehaviour 
{
	public Rigidbody2D rb;
	private bool canjump;
	// Use this for initialization
	void Start () {
		canjump = true;
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		//print (canjump);
		if (Input.GetKeyDown (KeyCode.Space) && canjump) {
			rb.AddForce (new Vector2 (0, 700));
			canjump = false;
		}
	}
	void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("ground") || other.gameObject.CompareTag ("spike")) {
			canjump = true;
		}
	}
}
