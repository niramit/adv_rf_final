﻿using UnityEngine;
using System.Collections;

public class GhostScript : MonoBehaviour, IMonster
{
	public Rigidbody2D rb;

	private float s;

	public Animator useAnimator;

	private int GhostHealth;

	public GameObject Health1;
	public GameObject Health2;
	public GameObject Health3;
	public GameObject Health4;
	public GameObject Health5;
	public GameObject Health6;
	public GameObject HealthBG;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		s = -2;
		GhostHealth = 6;
	}

	void Update () 
	{
		
		/*this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (s, 0);
		if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x < -4) 
		{
			this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (4, 0);
		}
		if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x > 4) {
			this.gameObject.GetComponent<Rigidbody2D> ().velocity -= new Vector2 (4, 0);
		}
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("next") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next") )
		{
			transform.rotation = Quaternion.Euler(0,180,0);
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			transform.rotation = Quaternion.Euler(0,0,0);
		}
		if (other.gameObject.CompareTag ("Player")) {
			useAnimator.SetBool ("hit", true);
		} 
	}
	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			useAnimator.SetBool ("hit", false);
		} 
		*/
	}

	public void hit()
	{
		GhostHealth--;
		if (GhostHealth == 5) 
		{
			Destroy (Health1.gameObject, 0.0f);
		} 
		if (GhostHealth == 4) 
		{
			Destroy (Health2.gameObject, 0.0f);
		} 
		if (GhostHealth == 3) 
		{
			Destroy (Health3.gameObject, 0.0f);
		} 
		if (GhostHealth == 2) 
		{
			Destroy (Health4.gameObject, 0.0f);
		} 
		if (GhostHealth == 1) 
		{
			Destroy (Health5.gameObject, 0.0f);
		} 
		if (GhostHealth == 0) 
		{
			Destroy (Health6.gameObject, 0.0f);
			Destroy (HealthBG.gameObject, 0.0f);
			Destroy (this.gameObject, 0.0f);
		} 
	}
}
