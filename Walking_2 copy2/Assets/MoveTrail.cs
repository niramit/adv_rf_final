﻿using UnityEngine;
using System.Collections;

public class MoveTrail : MonoBehaviour
{
	private float MoveSpeed = 100f;

	void Update () 
	{
		transform.Translate (Vector3.right * Time.deltaTime * MoveSpeed);
		Destroy (gameObject, 5);
	}

	void OnCollisionEnter2D (Collision2D other)
	{	
		Debug.Log ("tai si");
		IMonster mon;
		if (other.gameObject.GetComponent<IMonster>()!= null)
		{
			Debug.Log ("tai si");
			mon = other.gameObject.GetComponent(typeof(IMonster)) as IMonster;
			mon.hit ();
		}

	}

}
