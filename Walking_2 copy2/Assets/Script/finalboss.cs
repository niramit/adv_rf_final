﻿using UnityEngine;
using System.Collections;

public class finalboss : MonoBehaviour, IMonster 
{
	private int Health;
	public GameObject BallLeft;
	public Animator useAnimator;
	private float gunTime = 4.0f;
	private bool BallActive;
	public GameObject BallLauncher1;
	public GameObject BallLauncher2;
	public GameObject BallLauncher3;

	public GameObject Health1;
	public GameObject Health2;
	public GameObject Health3;
	public GameObject Health4;
	public GameObject Health5;
	public GameObject Health6;
	public GameObject Health7;
	public GameObject Health8;
	public GameObject Health9;
	public GameObject Health10;
	public GameObject Health11;
	public GameObject Health12;
	public GameObject Health13;
	public GameObject Health14;
	public GameObject Health15;
	public GameObject Health16;
	public GameObject Health17;
	public GameObject Health18;
	public GameObject Health19;
	public GameObject Health20;

	public GameObject HealthBG;

	void Start () 
	{
		BallActive = true;
		Health = 20;
	}

	void Update ()
	{
		if (BallActive == true) {
			/*useAnimator.SetBool ("hit", true);
			GameObject munObject = Instantiate (BallLeft, BallLauncher1.transform.position, Quaternion.identity) as GameObject;
			munObject.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine (gunWait ());

			GameObject munObject1 = Instantiate (BallLeft, BallLauncher2.transform.position, Quaternion.identity) as GameObject;
			munObject1.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine (gunWait ());

			GameObject munObject2 = Instantiate (BallLeft, BallLauncher3.transform.position, Quaternion.identity) as GameObject;
			munObject2.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine (gunWait ());
		} 
		else
		{
			useAnimator.SetBool ("hit", false);
		}
	}
	*/
		}
	}
	public void hit()
	{
		print ("hit");
		Health--;
		if (Health == 19) 
		{
			Destroy (Health1.gameObject, 0.0f);
		} 
		if (Health == 18) 
		{
			Destroy (Health2.gameObject, 0.0f);
		} 
		if (Health == 17) 
		{
			Destroy (Health3.gameObject, 0.0f);
		} 
		if (Health == 16) 
		{
			Destroy (Health4.gameObject, 0.0f);
		} 
		if (Health == 15) 
		{
			Destroy (Health5.gameObject, 0.0f);
		} 
		if (Health == 14) 
		{
			Destroy (Health6.gameObject, 0.0f);
		} 
		if (Health == 13) 
		{
			Destroy (Health7.gameObject, 0.0f);
		} 
		if (Health == 12) 
		{
			Destroy (Health8.gameObject, 0.0f);
		} 
		if (Health == 11) 
		{
			Destroy (Health9.gameObject, 0.0f);
		} 
		if (Health == 10) 
		{
			Destroy (Health10.gameObject, 0.0f);
		} 
		if (Health == 9) 
		{
			Destroy (Health11.gameObject, 0.0f);
		} 
		if (Health == 8) 
		{
			Destroy (Health12.gameObject, 0.0f);
		} 
		if (Health == 7) 
		{
			Destroy (Health13.gameObject, 0.0f);
		} 
		if (Health == 6) 
		{
			Destroy (Health14.gameObject, 0.0f);
		} 
		if (Health == 5) 
		{
			Destroy (Health15.gameObject, 0.0f);
		} 
		if (Health == 4) 
		{
			Destroy (Health16.gameObject, 0.0f);
		} 
		if (Health == 3) 
		{
			Destroy (Health17.gameObject, 0.0f);
		} 
		if (Health == 2) 
		{
			Destroy (Health18.gameObject, 0.0f);
		} 
		if (Health == 1) 
		{
			Destroy (Health19.gameObject, 0.0f);
		} 
		if (Health == 0) 
		{
			Destroy (HealthBG.gameObject, 0.0f);
			Destroy (Health20.gameObject, 0.0f);
			Destroy (this.gameObject, 0.0f);
		} 
	}

	IEnumerator gunWait()
	{
		yield return new WaitForSeconds(gunTime);
		BallActive = true;
	}
}
