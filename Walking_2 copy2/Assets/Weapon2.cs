﻿using UnityEngine;
using System.Collections;

public class Weapon2 : MonoBehaviour 
{

	public float fireRate = 0;
	public float Damage = 10;
	public LayerMask whatToHit;
	public Char2D LeftRightCode;

	public Transform BulletTrailPrefab;

	public Transform MuzzleFlashPrefab;

	float TimetoSpawnEffect = 0;
	public float EffectSpawnRate = 10;

	float timeToFire = 0;
	public Transform firePoint2;

	/*void Awake()
	{
		firePoint2 = gameobject.FindChild ("BulletLaunchPoint1");
		if (firePoint2 == null) 
		{
			Debug.LogError ("No Firepoint ! What?");
		}
	}
	*/

	void Start () 
	{
		//Camshake = GameMaster.GetComponent<CameraShaker> ();
	}

	void Update () 
	{
		if (fireRate == 0) 
		{
			if(Input.GetButtonDown ("Fire1"))
			{
				Debug.Log ("FIRE FIRE");
					Shoot();
			}	
			else if(Input.GetButton("Fire1") && Time.time > timeToFire)
			{
				Debug.Log ("FIRE FIRE2");
					timeToFire = Time.time + 1/fireRate;
					Shoot();
			}
		}
	}

	void Shoot()
	{
		Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
		Vector2 firePointPosition2 = new Vector2 (firePoint2.position.x, firePoint2.position.y);
		//RaycastHit2D hit = Physics2D.Raycast (firePointPosition2, mousePosition - firePointPosition2, 100, whatToHit);
		if (Time.time >= TimetoSpawnEffect) 
		{
			Effect ();
			TimetoSpawnEffect = Time.time + 1 / EffectSpawnRate;

		}
		Debug.DrawLine (firePointPosition2,(mousePosition-firePointPosition2) * 10, Color.cyan);
		/*if (hit.collider != null) 
		{
			Debug.DrawLine (firePointPosition2, hit.point, Color.red);
			Debug.Log ("We hit " + hit.collider.name + " and did " + Damage + "damage.");
		}
		*/

	}

	void Effect()
	{
		if (LeftRightCode.m_FacingRight == true) 
		{
			Debug.Log ("Normal Instantiate");
			Instantiate (BulletTrailPrefab, firePoint2.position, firePoint2.rotation);
		} else 
		{
			Debug.Log (" ELSEEEEEEEE ");
			Quaternion q = firePoint2.rotation;
			float temp = q.z;
			q.z = q.w;
			q.w = temp;

			Instantiate (BulletTrailPrefab, firePoint2.position, q);

		}
		print (firePoint2.rotation);
		print (firePoint2.rotation.x);
		print (firePoint2.rotation.y);
		print (firePoint2.rotation.z);
		Transform clone = Instantiate (MuzzleFlashPrefab, firePoint2.position, firePoint2.rotation) as Transform;
		clone.parent = firePoint2;
		float size = Random.Range (0.15f, 0.4f);
		clone.localScale = new Vector3 (size, size, size);
		Destroy (clone.gameObject, 0.05f);
	}
}
