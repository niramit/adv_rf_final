﻿using UnityEngine;
using System.Collections;

public class BatScript : MonoBehaviour, IMonster
{
	public Rigidbody2D rb;

	private int BatHealth;

	public GameObject Health1;
	public GameObject Health2;
	public GameObject Health3;
	public GameObject Health4;

	public GameObject HealthBG;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		BatHealth = 4;
	}

	void Update () 
	{
		
	}

	//Vector3 CalculateBatMovement()

	public void hit()
	{
		print ("hit");
		BatHealth--;
		if (BatHealth == 3) {
			Destroy (Health1.gameObject, 0.0f);
		}
		if (BatHealth == 2) {
			Destroy (Health2.gameObject, 0.0f);
		}
		if (BatHealth == 1) {
			Destroy (Health3.gameObject, 0.0f);
		} 
		if (BatHealth == 0) {
			Destroy (Health4.gameObject, 0.0f);
			Destroy (this.gameObject, 0.0f);
			Destroy (HealthBG.gameObject, 0.0f);
		}
	}
}
