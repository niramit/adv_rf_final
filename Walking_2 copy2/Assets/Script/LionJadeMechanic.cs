﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LionJadeMechanic : MonoBehaviour 
{
	public GameObject BallLeft;
	private bool BallActive;
	private float gunTime = 1.0f;
	public GameObject BallLauncher1;
	public GameObject BallLauncher2;
	public GameObject BallLauncher3;
	public Text TimerText;
	public float MyCoolTimer = 60;

	void Start () 
	{
		BallActive = true;
		//TimerText = GetComponent<Text> ();
	}

	void Update () 
	{
		if (BallActive == true && MyCoolTimer >=0) 
		{
			GameObject munObject = Instantiate (BallLeft, BallLauncher1.transform.position, Quaternion.identity) as GameObject;
			munObject.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine (gunWait ());

			GameObject munObject1 = Instantiate (BallLeft, BallLauncher2.transform.position, Quaternion.identity) as GameObject;
			munObject1.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine (gunWait ());

			GameObject munObject2 = Instantiate (BallLeft, BallLauncher3.transform.position, Quaternion.identity) as GameObject;
			munObject2.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine (gunWait ());
		}
			
	}

	void OnCollisionStay2D (Collision2D other)
	{
		/*if (other.gameObject.CompareTag ("ground")&& BallActive == true) 
		{
			GameObject munObject=Instantiate(BallLeft,BallLauncher1.transform.position,Quaternion.identity) as GameObject;
			munObject.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine(gunWait());

			GameObject munObject1=Instantiate(BallLeft,BallLauncher2.transform.position,Quaternion.identity) as GameObject;
			munObject1.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine(gunWait());

			GameObject munObject2=Instantiate(BallLeft,BallLauncher3.transform.position,Quaternion.identity) as GameObject;
			munObject2.name = "Play BallLeft ";
			BallActive = false;
			StartCoroutine(gunWait());

			//MyCoolTimer -= Time.deltaTime;
			//TimerText.text = MyCoolTimer.ToString ("f0"); 
			//print (MyCoolTimer);
		}*/
		if (other.gameObject.CompareTag ("ground")) 
		{
			Debug.Log ("StartCountTime");
			GameObject.Find ("CountdownText").GetComponent<Text> ().text = "Time Remaining : " + MyCoolTimer;
			MyCoolTimer -= Time.deltaTime;
			TimerText.text = MyCoolTimer.ToString ("f0"); 
		}
	}

	IEnumerator gunWait()
	{
			yield return new WaitForSeconds(gunTime);
			BallActive = true;
	}




}
